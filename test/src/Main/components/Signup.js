import { Button, Grid, Icon, Paper, Typography, TextField, makeStyles } from '@material-ui/core';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles'
import Blue from "@material-ui/core/colors/blue";
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import LinkedInIcon from '@material-ui/icons/LinkedIn';
import SignUpScreen from '../../assets/signupscreen.PNG';
import GoogleIcon from '../../assets/google.svg';
import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';

const blueTheme = createMuiTheme({
  palette: {
    primary: {
      main: Blue[700],
    },
  },
});
const useStyles = makeStyles(() => ({
  paperStyle: {
    paddingLeft: "100px",
    paddingTop: "42px",
    paddingBottom: "42px",
  },
  mainMargin: {
    marginLeft: "12px",
  },
  itemMargin: {
    marginTop: "4px",
  },
  signup: {
    paddingRight: "14px",
  },
  forgotPassword: {
    float: "right",
  },
  image: {
    width: "584px",
  },
  alignTypography: {
    textAlign: "center",
  },
  iconSize: {
    fontSize: "26px !important",
  },
  linkedinIcon: {
    fontSize: "26px !important",
    color: "#0062FF",
  },
}));
const SignUp = ()=>{
    const [userName, setUserName] = useState('');
    const [password, setPassword] = useState('');

    const history = useHistory();
    const classes = useStyles();

   return (
     <Grid container spacing={2}>
       <Grid item md={6}>
         <Paper elevation={0} className={classes.paperStyle}>
           <MuiThemeProvider theme={blueTheme}>
             <Typography variant="caption" color="primary">
               Sign In
             </Typography>
           </MuiThemeProvider>
           <Typography variant="h2" gutterBottom>
             Next Generation Employment Management
           </Typography>
           <Typography variant="subtitle1" color="textPrimary">
             {" "}
             Gigarena is a gig marketplace for DevOps work and a platform for
             companies to acess a wide pool of talent that are constantly
             getting better though contineous upskilling through technical
             challenges & task.
           </Typography>
           <MuiThemeProvider theme={blueTheme}>
             <TextField
               required
               label="UserName"
               variant="outlined"
               color="primary"
               helperText="please enter 8 characters"
               fullWidth
               value={userName}
               onChange={(e) => {
                 setUserName(e.target.value);
               }}
               className={classes.itemMargin}
             />
             <TextField
               id="outlined-password-input"
               label="Password"
               type="password"
               color="primary"
               helperText="please enter 8 characters"
               fullWidth
               value={password}
               onChange={(e) => setPassword(e.target.value)}
               autoComplete="current-password"
               variant="outlined"
               className={classes.itemMargin}
             />
             <Button
               variant="text"
               color="primary"
               className={classes.forgotPassword}
               endIcon={<ArrowForwardIosIcon />}
             >
               forgot password ?
             </Button>
             <Button
               variant="contained"
               fullWidth
               className={classes.signup}
               disabled={(userName && password).length < 8}
               size="large"
               color="primary"
               disableElevation
               disableRipple
               disableTouchRipple
               disableFocusRipple
               onClick={() => {
                 if (userName.length >= 8 && password.length >= 8) {
                   history.push({
                     pathname: "/dashboard",
                     state: { userName },
                   });
                 }
               }}
             >
               Sign up
             </Button>
           </MuiThemeProvider>
           <Typography className={classes.alignTypography}>or</Typography>
           <MuiThemeProvider theme={blueTheme}>
             <Grid container spacing={2}>
               <Grid item md={6}>
                 <Button
                   variant="outlined"
                   color="primary"
                   fullWidth
                   startIcon={
                     <Icon className={classes.iconSize}>
                       <img src={GoogleIcon} alt={GoogleIcon} />
                     </Icon>
                   }
                 >
                   Google
                 </Button>
               </Grid>
               <Grid item md={6}>
                 <Button
                   variant="outlined"
                   color="primary"
                   fullWidth
                   startIcon={<LinkedInIcon className={classes.linkedinIcon} />}
                 >
                   LinkedIn
                 </Button>
               </Grid>
             </Grid>
           </MuiThemeProvider>
           <MuiThemeProvider theme={blueTheme}>
             <Button
               variant="text"
               color="primary"
               className={classes.forgotPassword}
               endIcon={<ArrowForwardIosIcon />}
             >
               Don't have an account? Register
             </Button>
           </MuiThemeProvider>
         </Paper>
       </Grid>
       <Grid item md={6}>
         <img src={SignUpScreen} alt="logo" className={classes.image} />
       </Grid>
     </Grid>
   );
}

export default SignUp;
