import { Avatar, Box, Button, Card, CircularProgress, List, ListItem, ListItemText, ListItemAvatar, Typography, makeStyles } from "@material-ui/core";
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles'
import Blue from "@material-ui/core/colors/blue";
import AddIcon from '@material-ui/icons/Add';
import CapIcon from '../../assets/cap.png';
import React from 'react';

const blueTheme = createMuiTheme({
  palette: {
    primary: {
      main: Blue[700],
    },
  },
});


const useStyles = makeStyles((theme) => ({
  circular: {
    color: "#0062FF",
  },
  paddingBox: {
    padding: "16px",
  },
  textSize: {
    fontSize: "0.67rem",
  },
  cardMargin: {
    marginTop: "16px",
    marginLeft: "8px",
    marginRight: "126px",
    marginBottom: "16px",
    [theme.breakpoints.down("sm")]: {
      margin: "0",
    },
    [theme.breakpoints.up("md")]: {
      marginTop: "16px",
    marginLeft: "8px",
    marginRight: "48px",
    marginBottom: "16px",
    },
  },
}));
const skillsList = ['agilemethodoligies', 'userExperienceAnalytics'];
const PointSkillCard = () => {
    const classes = useStyles();

    return (
      <Card className={classes.cardMargin}>
        <Box
          position="relative"
          display="inline-flex"
          className={classes.paddingBox}
        >
          Points
          <MuiThemeProvider theme={blueTheme}>
            <CircularProgress
              color="primary"
              size="180px"
              variant="static"
              value={78}
            />
          </MuiThemeProvider>
          <Box
            top={4}
            left={76}
            bottom={4}
            right={4}
            position="absolute"
            display="flex"
            alignItems="center"
            justifyContent="center"
          >
            <Typography
              className={classes.textSize}
              variant="caption"
              component="div"
              color="textSecondary"
            >
              My points 7,85,283 out of 10,00,000
            </Typography>
          </Box>
        </Box>
        <Box className={classes.paddingBox}>
          Skills
          <MuiThemeProvider theme={blueTheme}>
            <List>
              {skillsList.map((skill) => (
                <ListItem key={skill}>
                  <ListItemAvatar color="primary">
                    <Avatar src={CapIcon} alt={CapIcon} />
                  </ListItemAvatar>
                  <ListItemText
                    primary={skill}
                  />
                </ListItem>
              ))}
            </List>
          </MuiThemeProvider>
          <MuiThemeProvider theme={blueTheme}>
            <Button
              variant="contained"
              color="primary"
              fullWidth
              startIcon={<AddIcon />}
            >
              Add skill
            </Button>
          </MuiThemeProvider>
        </Box>
      </Card>
    );
}

export default PointSkillCard;