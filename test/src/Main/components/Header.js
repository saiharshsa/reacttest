import React, { useState } from 'react';
import {Grid, Paper, Typography, makeStyles} from '@material-ui/core';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles'
import Blue from "@material-ui/core/colors/blue";
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import HeaderImg from '../../assets/headerImg.svg';

const blueTheme = createMuiTheme({
  palette: {
    primary: {
      main: Blue[700],
    },
  },
});

const useStyles = makeStyles((theme) => ({
  tabStyle: {
    marginLeft: "100px",
  },
  mainPaperStyle: {
     flexGrow: 1,
  },
  paperStyle: {
    background: "#ffcc99",
    marginLeft: "306px",
    marginRight: "125px",
    marginTop: "8px",
    [theme.breakpoints.down("sm")]: {
      margin: "0",
    },
    [theme.breakpoints.up("md")]: {
      marginLeft: "306px",
      marginRight: "52px",
      marginTop: "8px",
    },
  },
  image: {
    width: "200px",
    [theme.breakpoints.down("sm")]: {
      width: "100px",
    },
  },
}));

const Header=()=> {
  const [value, setValue] = useState(3);
  const classes = useStyles();

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <>
      <Paper square className={classes.mainPaperStyle}>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <MuiThemeProvider theme={blueTheme}>
              <Tabs
                className={classes.tabStyle}
                value={value}
                indicatorColor="primary"
                textColor="primary"
                onChange={handleChange}
              >
                <Tab key={Math.random()} label="All" />
                <Tab key={Math.random()} label="In Review" />
                <Tab key={Math.random()} label="Moderator" />
                <Tab key={Math.random()} label="Admin" />
              </Tabs>
            </MuiThemeProvider>
          </Grid>
        </Grid>
      </Paper>
      <Paper elevation={0} className={classes.paperStyle}>
        <Grid container spacing={2}>
          <Grid item md={9} xs={9}>
            <div className={classes.tabStyle}>
              <Typography variant="h3">
                Advance Yourself. Earn Recognition Points.
              </Typography>
              <Typography variant="h3">And Help Others Too.</Typography>
              <Typography variant="subtitle1">
                Meet your goals. Learn something new. Collabrate with others.
                This week there are 12 new quests available{" "}
              </Typography>
            </div>
          </Grid>
          <Grid item md={3} xs={3}>
            <img src={HeaderImg} alt="logo" className={classes.image} />
          </Grid>
        </Grid>
      </Paper>
    </>
  );
};
export default Header;