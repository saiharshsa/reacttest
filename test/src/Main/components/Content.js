import {
  Avatar,
  Button,
  Chip,
  CircularProgress,
  Card,
  Divider,
  Grid,
  Icon,
  List,
  ListItem,
  ListItemText,
  ListItemAvatar,
  makeStyles,
  Paper,
  Stepper,
  Step,
  StepLabel,
  StepContent,
  Typography,
} from "@material-ui/core";
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles'
import Blue from "@material-ui/core/colors/blue";
import React, { useState} from 'react';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import AccessTimeIcon from '@material-ui/icons/AccessTime';
import StarIcon from '@material-ui/icons/Star';
import GreenStarIcon from '../../assets/star.png';
import TimeIcon from '../../assets/time.png';
import MaleIcon from '../../assets/male.png';
import FeMaleIcon from '../../assets/female.png';

const blueTheme = createMuiTheme({
  palette: {
    primary: {
      main: Blue[700],
    },
  },
});

const useStyles = makeStyles((theme) => ({
  mainCard: {
    marginLeft: "182px",
    marginRight: "2px",
    marginTop: "16px",
    [theme.breakpoints.down("sm")]: {
      marginLeft: "0",
      marginRight: "0",
      marginBottom: '0',
      marginTop: "16px",
    },
  },
  marginCard: {
    marginLeft: "42px",
  },
  alignCenter: {
    textAlign: "center",
  },
  marginleft: {
    marginLeft: "4px",
  },
  root: {
    width: "100%",
  },
  button: {
    marginTop: "8px",
    marginRight: "8px",
  },
  actionsContainer: {
    marginBottom: "16px",
  },
  resetContainer: {
    padding: "24px",
  },
  chip: {
    marginBottom: "16px",
    marginTop: "16px",
    marginLeft: "16px",
    color: "#fff",
  },
  circular: {
    marginLeft: "30px"
  },
  createdCard1: {
    marginTop: "8px",
    marginBottom: "12px",
    marginLeft: "24px",
  },
  icon: {
    marginTop: "10px",
    marginBottom: "12px",
    marginLeft: "24px",
  },
  floatButton: {
    float: "right",
  },
}));

const steps = [
  "Signup for microsoft Azure Cloud",
  "Complete weekly security patches",
  "complete the security patches updating of your customer and help them to stay secure from cyber attacks",
  "Launch project in dev environment and send link to complete",
];
const Content = () =>{
    const [activeStep, setActiveStep] = useState(0);
    const classes = useStyles();

    const handleNext = () => {
        setActiveStep((prevActiveStep) => prevActiveStep + 1);
      };
    
      const handleBack = () => {
        setActiveStep((prevActiveStep) => prevActiveStep - 1);
      };
    
      const handleReset = () => {
        setActiveStep(0);
      };

    return (
      <>
        <Card className={classes.mainCard}>
          <MuiThemeProvider theme={blueTheme}>
            <Chip
              label="Cloud Engineering"
              className={classes.chip}
              color="primary"
            />
          </MuiThemeProvider>
          <Grid container>
            <Grid item sm={1} md={1}>
              <MuiThemeProvider theme={blueTheme}>
                <CircularProgress
                  className={classes.circular}
                  variant="static"
                  size="12px"
                  value={80}
                  color="primary"
                />
              </MuiThemeProvider>
            </Grid>
            <Grid item sm={9} md={9}>
              <Typography variant="h6"> Start an azure demo project</Typography>
            </Grid>
            <Grid item sm={2} md={1}>
              <MuiThemeProvider theme={blueTheme}>
                <Icon>
                  <StarIcon color="primary" />
                </Icon>
                <Typography variant="caption" color="primary">
                  {" "}
                  20,000
                </Typography>{" "}
              </MuiThemeProvider>
            </Grid>
          </Grid>
          <Typography className={classes.marginCard} variant="subtitle1">
            Microsoft Azure has decided to partner with us and supply us with a
            company wide support for our engineer with active support. Begin
            your journey to upskill yourself to be an Azure cloud engineer
          </Typography>
          <MuiThemeProvider theme={blueTheme}>
            <Stepper
              activeStep={activeStep}
              orientation="vertical"
              color="primary"
            >
              {steps.map((step, index) => (
                <Step key={step}>
                  <StepLabel>{step}</StepLabel>
                  <StepContent>
                    <div className={classes.actionsContainer}>
                      <div>
                        <Button
                          disabled={activeStep === 0}
                          onClick={handleBack}
                          className={classes.button}
                        >
                          Back
                        </Button>
                        <Button
                          variant="contained"
                          color="primary"
                          onClick={handleNext}
                          className={classes.button}
                        >
                          {activeStep === steps.length - 1 ? "Finish" : "Next"}
                        </Button>
                      </div>
                    </div>
                  </StepContent>
                </Step>
              ))}
            </Stepper>
          </MuiThemeProvider>
          {activeStep === steps.length && (
            <Paper square elevation={0} className={classes.resetContainer}>
              <Typography>
                All steps completed - you&apos;re finished want to reset?
              </Typography>
              <Button
                variant="contained"
                color="primary"
                onClick={handleReset}
                className={classes.button}
              >
                Reset
              </Button>
            </Paper>
          )}
          <Divider />
          <Grid container>
            <Grid item md={4} xs={12} className={classes.alignCenter}>
              <List>
                <ListItem key={Math.random()} className={classes.marginleft}>
                  <ListItemAvatar>
                    <Avatar src={GreenStarIcon} alt={GreenStarIcon} />
                  </ListItemAvatar>
                  <ListItemText primary="Cyber Defender" secondary="badge" />
                </ListItem>
              </List>
            </Grid>
            <Grid item md={4} xs={12} className={classes.alignCenter}>
              <List>
                <ListItem key={Math.random()}>
                  <ListItemAvatar>
                    <Avatar src={TimeIcon} alt={TimeIcon} />
                  </ListItemAvatar>
                  <ListItemText primary="2 days" secondary="time left" />
                </ListItem>
              </List>
            </Grid>
            <Grid item md={4} xs={12} className={classes.alignCenter}>
              <List>
                <ListItem key={Math.random()}>
                  <ListItemAvatar>
                    <Avatar src={MaleIcon} alt={MaleIcon} />
                  </ListItemAvatar>
                  <ListItemAvatar>
                    <Avatar src={FeMaleIcon} alt={FeMaleIcon} />
                  </ListItemAvatar>
                  <ListItemText primary="2 People" secondary="Moderators" />
                </ListItem>
              </List>
            </Grid>
          </Grid>
          <Divider />
          <Typography variant="subtitle2" className={classes.createdCard1}>
            Created: 12 January 2019
          </Typography>
        </Card>
        <Card className={classes.mainCard}>
          <Grid container>
            <Grid item sm={1} md={1}>
              <MuiThemeProvider theme={blueTheme}>
                <CircularProgress
                  className={classes.circular}
                  variant="static"
                  size="12px"
                  color="primary"
                  value={80}
                />
              </MuiThemeProvider>
            </Grid>
            <Grid item sm={9} md={9}>
              <Typography variant="h6">
                {" "}
                Attend Company Annual Dinner 2020
              </Typography>
            </Grid>
            <Grid item sm={2} md={1}>
              <MuiThemeProvider theme={blueTheme}>
                <Icon>
                  <StarIcon color="primary" />
                </Icon>
                <Typography variant="caption" color="primary">
                  {" "}
                  20,000
                </Typography>{" "}
              </MuiThemeProvider>
            </Grid>
          </Grid>
          <Grid container>
            <Grid item md={10} sm={10}>
              <Typography className={classes.marginCard} variant="subtitle1">
                Our company 10th annual dinner is happening this Friday.Please
                mark your attendance for the dinner.
              </Typography>
            </Grid>
            <Grid item md={2} sm={2}>
              <MuiThemeProvider theme={blueTheme}>
                <Button variant="outlined" color="primary">
                  Start
                </Button>
              </MuiThemeProvider>
            </Grid>
          </Grid>
          <Divider />
          <Grid container>
            <Grid item md={6} xs={12} className={classes.alignCenter}>
              <List>
                <ListItem key={Math.random()} className={classes.marginleft}>
                  <ListItemAvatar>
                    <Avatar src={GreenStarIcon} alt={GreenStarIcon} />
                  </ListItemAvatar>
                  <ListItemText primary="Loyalty Dinner" secondary="badge" />
                </ListItem>
              </List>
            </Grid>
            <Grid item md={6} xs={12} className={classes.alignCenter}>
              <List>
                <ListItem key={Math.random()}>
                  <ListItemAvatar>
                    <Avatar src={TimeIcon} alt={TimeIcon} />
                  </ListItemAvatar>
                  <ListItemText primary="5 days" secondary="time left" />
                </ListItem>
              </List>
            </Grid>
          </Grid>
          <Divider />
          <Grid container>
            <Grid item md={4} xs={12}>
              <Icon className={classes.icon}>
                <AccessTimeIcon />
              </Icon>
              12 January 2019
            </Grid>
            <Grid item md={8} xs={12}>
              <MuiThemeProvider theme={blueTheme}>
                <Button
                  variant="text"
                  color="primary"
                 className={classes.floatButton}
                  endIcon={<ArrowForwardIosIcon />}
                >
                  Submit Quest
                </Button>
              </MuiThemeProvider>
            </Grid>
          </Grid>
        </Card>
      </>
    );
}

export default Content;