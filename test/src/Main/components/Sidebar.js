import {
  Drawer,
  IconButton,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  makeStyles,
  ListSubheader,
  ListItemIcon,
} from "@material-ui/core";
import MenuIcon from "@material-ui/icons/Menu";
import AssessmentIcon from '@material-ui/icons/Assessment';
import HomeIcon from '@material-ui/icons/Home';
import ShowChartIcon from '@material-ui/icons/ShowChart';
import QuestionAnswerIcon from '@material-ui/icons/QuestionAnswer';
import GroupWorkIcon from '@material-ui/icons/GroupWork';
import BusinessIcon from '@material-ui/icons/Business';
import NotificationsIcon from '@material-ui/icons/Notifications';
import GamesIcon from '@material-ui/icons/Games';
import SettingsIcon from '@material-ui/icons/Settings';
import MoreHorizIcon from '@material-ui/icons/MoreHoriz';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import Avatar from '@material-ui/core/Avatar';
import UserImage from '../../assets/user.jpg';
import React, {useState} from 'react';

const useStyles = makeStyles((theme) => ({
  itemText: {
    color: "#fff",
    fontSize: "24px",
  },
  Color:{
    color: "#fff",
  },
  background: {
    backgroundColor: "#252631",
  },
  listItem: {
    color: "#fff",
  },
}));

const Sidebar = (userName) => {
    const sideList = [
      { text: "Home", icon: <HomeIcon /> },
      { text: "Notifications", icon: <NotificationsIcon /> },
      { text: "NewsFeed", icon: <ShowChartIcon /> },
      { text: "Quests", icon: <QuestionAnswerIcon /> },
      { text: "GoalSettings", icon: <SettingsIcon /> },
      { text: "Rewards", icon: <KeyboardArrowUpIcon /> },
      { text: "Colloborate", icon: <GroupWorkIcon /> },
      { text: "Games", icon: <GamesIcon /> },
      { text: "Organization", icon: <BusinessIcon /> },
      { text: "Reports", icon: <AssessmentIcon /> },
    ];
    const [open, setOpen] = useState(false);
    const classes = useStyles();
    return (
      <div>
        <IconButton onClick={() => setOpen(true)} color="inherit">
          <MenuIcon />
        </IconButton>

        <Drawer
          open={open}
          classes={{
            paper: classes.background,
          }}
          onClose={() => setOpen(false)}
        >
          <List>
            <ListSubheader className={classes.itemText}>SelDrvn</ListSubheader>
            {sideList.map(({ text, icon }) => (
              <ListItem key={text}>
                <ListItemIcon className={classes.Color}>{icon}</ListItemIcon>
                <ListItemText
                  key={text}
                  primary={text}
                  className={classes.itemText}
                />
                <ListItemIcon className={classes.Color}>
                  <KeyboardArrowUpIcon />
                </ListItemIcon>
              </ListItem>
            ))}
          </List>
          <List>
            <ListItem key={Math.random()} className={classes.listItem}>
              <ListItemAvatar>
                <Avatar
                  src={UserImage}
                  alt={UserImage}
                  className={classes.background}
                />
              </ListItemAvatar>
              <ListItemText
                primary={Object.values(userName)}
                secondary={`admin`}
                className={classes.Color}
                secondaryTypographyProps={{
                  color: "#fff",
                }}
              />
              <ListItemIcon className={classes.Color}>
                <MoreHorizIcon />
              </ListItemIcon>
            </ListItem>
          </List>
        </Drawer>
      </div>
    );

}

export default Sidebar;