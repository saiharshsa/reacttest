import { IconButton, Paper, Grid, makeStyles } from "@material-ui/core";
import ClearIcon from '@material-ui/icons/Clear';
import React, {useState}  from 'react';
import Content from '../components/Content';
import Sidebar from '../components/Sidebar';
import PointSkillCard from "../components/PointSkillCard";
import Header from "../components/Header";

const useStyles = makeStyles((theme) => ({
  action: {
    [theme.breakpoints.down("sm")]: {
      margin: "0",
    },
  },
  background: {
    backgroundColor: "#F9F9F9",
  },
  icon: {
    color: "#fff",
  },
  text: {
    backgroundColor: " #1976D2",
    textAlign: "center",
    color: "#fff",
  },
}));

const DashBoardPage = ({location : {state : { userName}}})=> {
    const [buttonDisable, setButtonDisable] = useState(true);
    const classes = useStyles();

return (
  <div className={classes.background}>
    {userName && (
      <>
        <Grid container>
          <Grid item xs={1} className={buttonDisable ? classes.text : null}>
            <Sidebar userName={userName} />
          </Grid>
          <Grid item xs={11}>
            {buttonDisable ? (
              <Paper className={classes.text}>
                Starting from 18 March 2020, all employee of Nettium and Selfdrv
                must work from home due to covid-19
                <IconButton
                  className={classes.icon}
                  onClick={() => {
                    setButtonDisable(false);
                  }}
                >
                  <ClearIcon />
                </IconButton>
              </Paper>
            ) : null}
          </Grid>
        </Grid>
        <Grid container spacing={2}>
          <Grid item md={12}>
            <Header />
          </Grid>
        </Grid>
        <Grid container spacing={2}>
          <Grid item md={1}></Grid>
          <Grid item md={7} xs={12}>
            <Content />
          </Grid>
          <Grid item md={4} xs={12} className={classes.action}>
            <PointSkillCard />
          </Grid>
        </Grid>
      </>
    )}
  </div>
);
}

export default DashBoardPage;