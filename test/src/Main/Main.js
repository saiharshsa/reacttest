import React from 'react';
import { BrowserRouter, Switch,  Route } from 'react-router-dom';

import SignupPage from './containers/SignupPage';
import DashBoardPage from './containers/DashBoardPage';


const Main = ()=>{
   return (
     <BrowserRouter>
       <Switch>
         <Route component={SignupPage} exact path="/" />
         <Route component={DashBoardPage} path="/dashboard" />
       </Switch>
     </BrowserRouter>
   );
}

export default Main;
